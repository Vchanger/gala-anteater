%define debug_package %{nil}

Name:            gala-anteater
Version:         1.0.1
Release:         2
Summary:         A time-series anomaly detection platform for operating system.
License:         MulanPSL2
URL:             https://gitee.com/openeuler/gala-anteater
Source:          %{name}-%{version}.tar.gz
BuildRoot:       %{_builddir}/%{name}-%{version}
BuildRequires:   procps-ng python3-setuptools
Requires:        python3-gala-anteater = %{version}-%{release}

Patch1:        Add-disk-throughput-detector.patch
Patch2:        Update-TCP-Establish-Model-Add-Nic-Loss-Detector.patch
Patch3:        add-chinese-descriptions.patch
Patch4:        remove-sys-level-config-param.patch
Patch5:        add-systemd-service-for-anteater.patch
Patch6:        fix-str2enum-bug-data-query-refactor.patch

%description
Abnormal detection module for A-Ops project

%package -n python3-gala-anteater
Summary:         Python3 package of gala-anteater
Requires:        python3-APScheduler python3-kafka-python python3-joblib python3-numpy
Requires:        python3-pandas python3-requests python3-scikit-learn python3-pytorch

%description -n python3-gala-anteater
Python3 package of gala-anteater

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install

%pre
if [ -f "%{_unitdir}/gala-anteater.service" ] ; then
        systemctl enable gala-anteater.service || :
fi

%post
%systemd_post gala-anteater.service

%preun
%systemd_preun gala-anteater.service

%postun
%systemd_postun_with_restart gala-anteater.service

%files
%doc README.md
%license LICENSE
%{_bindir}/gala-anteater
%config(noreplace) %{_sysconfdir}/%{name}/config/gala-anteater.yaml
%config(noreplace) %{_sysconfdir}/%{name}/config/log.settings.ini
%config(noreplace) %{_sysconfdir}/%{name}/config/module/app_sli_rtt.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/proc_io_latency.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/sys_io_latency.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/sys_tcp_establish.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/sys_tcp_transmission_latency.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/sys_tcp_transmission_throughput.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/disk_throughput.json
%config(noreplace) %{_sysconfdir}/%{name}/config/module/sys_nic_loss.json
/usr/lib/systemd/system/gala-anteater.service

%files -n python3-gala-anteater
%{python3_sitelib}/anteater/*
%{python3_sitelib}/gala_anteater-*.egg-info


%changelog
* Tue Jan 17 2023 Zhen Chen <chenzhen126@huawei.com> - 1.0.1-2
- fix str2enum bug & data query refactor
- add systemd service for anteater
- remove 'sys-level' config param
- add chinese descriptions
- Update TCP Establish Model & Add Nic Loss Detector
- Add disk throughput detector

* Wed Nov 30 2022 Li Zhenxing <lizhenxing11@huawei.com> - 1.0.1-1
- Add sys level anomaly detection and cause inference

* Tue Nov 22 2022 Li Zhenxing <lizhenxing11@huawei.com> - 1.0.0-2
- Updates anomaly detection model and imporves cause inference result

* Sat Nov 12 2022 Zhen Chen <chenzhen126@huawei.com> - 1.0.0-1
- Package init
